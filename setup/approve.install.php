<?php
/**
 * Installation handler
 *
 * @package approve
 * @version 0.1
 * @author y-ea.ru
 * @copyright Copyright (c) y-ea.ru
 * @license BSD
 */

defined('COT_CODE') or die('Wrong URL');

require_once cot_incfile('comments', 'plug');

global $db_com;

// Add field if missing
if (!$db->fieldExists($db_com, "com_state"))
{
	$db->query("ALTER TABLE `$db_com` ADD COLUMN `com_state` INT( 11 ) NOT NULL DEFAULT '0'");
}