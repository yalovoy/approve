<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=page.list.loop
Tags=page.list.tpl:{LIST_ROW_COMMENTS}
[END_COT_EXT]
==================== */

/**
 * Comments approve for Cotonti
 *
 * @package approve
 * @version 1.0
 * @author y-ea.ru
 * @copyright Copyright (c) y-ea.ru
 * @license BSD
 */

defined('COT_CODE') or die('Wrong URL');

require_once cot_incfile('approve', 'plug');

$page_urlp = empty($pag['page_alias']) ? array('c' => $pag['page_cat'], 'id' => $pag['page_id']) : array('c' => $pag['page_cat'], 'al' => $pag['page_alias']);
$t->assign(array(
	'LIST_ROW_COMMENTS' => cot_approve_comments_link('page', $page_urlp, 'page', $pag['page_id'], $c, $pag),
	'LIST_ROW_COMMENTS_COUNT' => cot_approve_comments_count('page', $pag['page_id'], $pag)
));
